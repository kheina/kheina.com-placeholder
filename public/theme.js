theme = 'kheina';
accents = 'none';
mascot = 0;

try
{
	setThemeOnLoad();
	setAccentsOnLoad();
	//randomMascot();
}
finally
{ }

function setThemeFromMenu()
{
	// get theme data from menu
	theme = document.getElementById('theme').value;
	// set css
	document.documentElement.className = theme + ' ' + accents;
	// set cookie for next time
	setCookie('theme', theme);
}

function setThemeOnLoad()
{
	// get theme data from cookie
	theme = getCookie('theme');
	// set css
	document.documentElement.className = theme + ' ' + accents;
}

function setThemeValue()
{ document.getElementById('theme').value = theme; }

function setAccentsFromMenu()
{
	// get accents data from menu
	accents = document.getElementById('accents').value;
	// set css
	document.documentElement.className = theme + ' ' + accents;
	// set cookie for next time
	setCookie('accents', accents);
}

function setAccentsOnLoad()
{
	// get accents data from cookie
	accents = getCookie('accents');
	// set css
	document.documentElement.className = theme + ' ' + accents;
}

function setAccentsValue()
{ document.getElementById('accents').value = accents; }

function randomMascot()
{
	// mascots = GET /mascots.json
	fetch('/mascots.json');

	// mascot = RAND(mascots.count);

	// set mascot to mascot_[num].png
	// document.querySelector('img.mascot').src = 'mascot_' + mascot + '.png';
}

function nextMascot()
{
	// mascot++;
	// if (mascot >= mascots)
	// { mascot = 0; }

	// set mascot to mascot_[num].png
	// document.querySelector('img.mascot').src = 'mascot_' + mascot + '.png';
}

function onLoaded()
{
	let content = document.querySelector('div#content');
	new ResizeSensor(content, function()
	{
		//console.log('top', content.offsetTop, 'content', content.clientHeight, 'window', window.innerHeight);
		if (content.offsetTop - content.clientHeight / 2 <= 128)
		{
			content.className = 'topy';
			if (content.clientHeight + 256 < window.innerHeight)
			{ content.className = 'centery'; }
		}
	});
	setThemeValue();
	setAccentsValue();
}


function setCookie(name, value)
{ document.cookie=name + '=' + escape(value) + '; path=/'; }

function getCookie(cname)
{
	var name = cname + '=';
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
		{ c = c.substring(1); }
		if (c.indexOf(name) == 0)
		{ return c.substring(name.length, c.length); }
	}
	return '';
}

function ResizeSensor(element, callback)
{ // https://stackoverflow.com/a/47965966
	let zIndex = parseInt(getComputedStyle(element));
	if(isNaN(zIndex)) { zIndex = 0; };
	zIndex--;

	let expand = document.createElement('div');
	expand.style.position = 'absolute';
	expand.style.left = '0px';
	expand.style.top = '0px';
	expand.style.right = '0px';
	expand.style.bottom = '0px';
	expand.style.overflow = 'hidden';
	expand.style.zIndex = zIndex;
	expand.style.visibility = 'hidden';

	let expandChild = document.createElement('div');
	expandChild.style.position = 'absolute';
	expandChild.style.left = '0px';
	expandChild.style.top = '0px';
	expandChild.style.width = '10000000px';
	expandChild.style.height = '10000000px';
	expand.appendChild(expandChild);

	let shrink = document.createElement('div');
	shrink.style.position = 'absolute';
	shrink.style.left = '0px';
	shrink.style.top = '0px';
	shrink.style.right = '0px';
	shrink.style.bottom = '0px';
	shrink.style.overflow = 'hidden';
	shrink.style.zIndex = zIndex;
	shrink.style.visibility = 'hidden';

	let shrinkChild = document.createElement('div');
	shrinkChild.style.position = 'absolute';
	shrinkChild.style.left = '0px';
	shrinkChild.style.top = '0px';
	shrinkChild.style.width = '200%';
	shrinkChild.style.height = '200%';
	shrink.appendChild(shrinkChild);

	element.appendChild(expand);
	element.appendChild(shrink);

	function setScroll()
	{
		expand.scrollLeft = 10000000;
		expand.scrollTop = 10000000;

		shrink.scrollLeft = 10000000;
		shrink.scrollTop = 10000000;
	};
	setScroll();

	let size = element.getBoundingClientRect();

	let currentWidth = size.width;
	let currentHeight = size.height;

	let onScroll = function()
	{
		let size = element.getBoundingClientRect();

		let newWidth = size.width;
		let newHeight = size.height;

		if(newWidth != currentWidth || newHeight != currentHeight)
		{
			currentWidth = newWidth;
			currentHeight = newHeight;

			callback();
		}

		setScroll();
	};

	expand.addEventListener('scroll', onScroll);
	shrink.addEventListener('scroll', onScroll);
};
